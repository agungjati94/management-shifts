const Mongoose = require('mongoose'),
    shiftSchm = new Mongoose.Schema({
        created: {
            name: String,
            username: String
        },
        statuses: {
            published: Boolean,
            statusdate: Date
        },
        days: [
            {
                name: String,
                limithour: Number,
                difhour :  Number,
                shifts: [{
                    worker: { name: String, username: String },
                    startdate: Date,
                    enddate: Date
                }]
            }
        ]
    });

module.exports = Mongoose.model('shifts', shiftSchm)