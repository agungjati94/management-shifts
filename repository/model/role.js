const Mongoose = require('mongoose'),
    roleSchm = new Mongoose.Schema({
        name: String
    });

module.exports = Mongoose.model('roles', roleSchm)