const Mongoose =  require('mongoose'),
    userSchm = new Mongoose.Schema({
        name: String,
        username: String,
        password: String,
        role: {
            id: Mongoose.Schema.Types.ObjectId,
            name: String
        }
    });

  module.exports = Mongoose.model('users', userSchm)