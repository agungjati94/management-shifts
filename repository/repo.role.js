const Role = require("./model/role")

class RoleRepo {
    
    Save(name) {
        this.role = new Role({
            name: name
        })

        return this.role.save()
    }

    Get() {
        return Role.find()
    }

    GetOne(name) {
        return Role.find({ name : name })
    }
}

module.exports = RoleRepo