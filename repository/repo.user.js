const User = require("./model/user"),
    mongoose = require('mongoose')

class UserRepo {

    Save({ name , username, password, role }) {
        this.user = new User({
            name: name,
            username: username,
            password: password,
            role: {
                id: mongoose.Types.ObjectId(role.id),
                name : role.name
            }
        })
        return this.user.save()
    }

    Get() {
        return User.find()
    }
}

module.exports = UserRepo