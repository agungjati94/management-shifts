const config = require('./config.database'),
    mongoose = require('mongoose')

let message = null
mongoose.connect(config.url, { useNewUrlParser: true });
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open',  function() {
    message = "connected"
  console.log("Connected to database")
});

module.exports = Promise.all([message])